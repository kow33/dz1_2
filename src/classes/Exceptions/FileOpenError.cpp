#include <utility>

//
// Created by Михаил Кочетков on 31/10/2018.
//

#include "FileOpenError.h"

const std::string FileOpenError::what() const {
    return "File \"" + _fileName + "\" open error";
}

FileOpenError::FileOpenError(std::string fileName) : _fileName(std::move(fileName)) {}
