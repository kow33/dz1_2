//
// Created by Михаил Кочетков on 01/11/2018.
//

#ifndef DZ1_DATA_H
#define DZ1_DATA_H

#pragma once

#include <string>
#include <vector>
#include <fstream>

struct Data {
    char surname[1024];
    int autoMarkCode;
    char autoMark[1024]; // finding
    char requiredFuelMark[1024]; // finding
    int enginePower; // finding
    int tankVolume;
    double fuelLeft;
    double oilVolume;

    Data();

    std::vector<std::string> ToVectorString();

    static std::vector<std::string> VarNamesToVector();

    friend std::istream &operator>>(std::istream &is, Data &data);
};


#endif //DZ1_DATA_H
