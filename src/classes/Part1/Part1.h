//
// Created by Михаил Кочетков on 29/10/2018.
//

#ifndef DZ1_FUNCTIONS_H
#define DZ1_FUNCTIONS_H

#pragma once

#include <fstream>
#include <iterator>
#include <limits>
#include <algorithm>
#include <functional>
#include <vector>
#include "../Exceptions/FileOpenError.h"
#include "../Operation/Operation.h"

class Part1 {
public:
    static void CreateData();
    static void CreateDataByIter();

    template <class T>
    static void LoadData(T &to);

    template <class T>
    static void LoadDataByIter(T &to);

    template <class T>
    static void ResultData(const T &from);

    template <class T>
    static void ResultDataByIter(const T &from);

    template <class T>
    static void Modify(T &container);

    template <class T>
    static void ModifyByForEach(T &container, Operation &operation);

    template <class Iterator>
    static void Modify(Iterator begin, Iterator end);

    static void Modify();
};

template<class T>
void Part1::LoadData(T &to) {
    std::ifstream file("src/files/part1/data.txt");

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    std::string buf;
    while (getline(file, buf)) {
        to.emplace_back(stoi(buf));
    }
}

template<class T>
void Part1::LoadDataByIter(T &to) {
    std::ifstream file("src/files/part1/data.txt");

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/data.txt");
    }

    copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), back_inserter(to));

    file.close();
}


template <class T>
void Part1::ResultData(const T &from) {
    std::ofstream file("src/files/part1/result_data.txt");

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/result_data.txt");
    }

    for (const auto &item : from) {
        file << item << std::endl;
    }

    file.close();
}

template <class T>
void Part1::ResultDataByIter(const T &from) {
    std::ofstream file("src/files/part1/result_data.txt");

    if (!file.is_open()) {
        throw FileOpenError("src/files/part1/result_data.txt");
    }

    copy(from.begin(), from.end(), std::ostream_iterator<int>(file, "\n"));

    file.close();
}

template <class T>
void Part1::Modify(T &container) {
    int Sum = 0;
    int size = container.size();

    int sum = 0;

    for (const auto &item : container) {
        Sum += item;
        if (item < 0) {
            sum += item;
        }
    }

    for (auto &item : container) {
        item += sum / 2;
    }

    container.emplace_back(Sum);
    container.emplace_back(Sum / size);
}

template <class T>
void Part1::ModifyByForEach(T &container, Operation &operation) {
    operation.variant = 0;
    operation = std::for_each(container.begin(), container.end(), operation);

    operation.sum /= 2;

    operation.variant = 1;
    std::for_each(container.begin(), container.end(), operation);
}

template <class Iterator>
void Part1::Modify(Iterator begin, Iterator end) {
    int sum = 0;

    auto size = end - begin;

    for (; begin != end; ++begin) {
        if (*begin < 0) {
            sum += *begin;
        }
    }

    sum /= 2;
    begin -= size;

    for (; begin != end; begin++) {
        *begin += sum;
    }
}

#endif //DZ1_FUNCTIONS_H
