#include <iostream>
#include <vector>
#include "src/classes/Menu/Menu.h"

using namespace std;

int main() {
    Menu menu;
    menu.run();

    return 0;
}